package by.golovin.task.reports;

import java.io.FileOutputStream;

public class ReportsBuilder {

    ReportBuilder reportBuilder = new ReportBuilder();
    ReportTemplateBuilder reportTemplateBuilder = new ReportTemplateBuilder()
            .documentPath("/.xls")
            .documentName(".xls")
            .outputType(ReportOutputType.xls)
            .readFileFromPath();
reportBuilder.template(reportTemplateBuilder.build());
    BandBuilder bandBuilder = new BandBuilder();
    ReportBand staff= bandBuilder.name("Staff")
            .query("", "sql")
            .build();
reportBuilder.band(staff);
    Report report = reportBuilder.build();

    Reporting reporting = new Reporting();
reporting.setFormatterFactory(new DefaultFormatterFactory());
reporting.setLoaderFactory(
        new DefaultLoaderFactory().setSqlDataLoader(new SqlDataLoader(datasource)));

    ReportOutputDocument reportOutputDocument = reporting.runReport(
            new RunParams(report), new FileOutputStream("report.xls"));
}
