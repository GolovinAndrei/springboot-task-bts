package by.golovin.task.crud.service;

import by.golovin.task.api.model.WorkedHours;
import by.golovin.task.api.model.Worker;
import by.golovin.task.crud.repositories.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class WorkerService {

    @Autowired
   private WorkerRepository workerRepository;

    public Worker findWorkerById(Long id) {
        if (id == null) {
            return null;
        }
        return workerRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public void deleteWorker(Long id) throws IOException {
        if (id == null) {
            throw new IOException("There is no worker with this id");
        } else {
            workerRepository.deleteById(id);
        }
    }


    public List<Worker> findAll() {
        return workerRepository.findAll();
    }

    public void saveWorker (Worker worker) throws IOException {
        if (worker == null) {
            throw new IOException("The value is empty!");
        } else {
            workerRepository.save(worker);
        }
    }

    public List<WorkedHours> ownWorkedHours (Long workerId){
        if (workerId == null) {
            return null;
        }
        return findWorkerById(workerId).getWorkedHours();
    }

}

