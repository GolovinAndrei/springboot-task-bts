package by.golovin.task.crud.service;

import by.golovin.task.api.model.Department;
import by.golovin.task.crud.repositories.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department findDepartmentById(Long id) {
        if (id == null) {
            return null;
        }
        return departmentRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public void deleteDepartment (Long id) throws IOException {
        if (id == null) {
            throw new IOException("There is no department with this id");
        } else {
            departmentRepository.deleteById(id);
        }
    }

    public void saveDepartment(Department department) throws IOException {
        if (department == null) {
            throw new IOException("The value is empty!");
        } else {
            departmentRepository.save(department);
        }
    }


    public List<Department> findAll() {
        return departmentRepository.findAll();
    }
}
