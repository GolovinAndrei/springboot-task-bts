package by.golovin.task.crud.service;

import by.golovin.task.api.model.WorkedHours;
import by.golovin.task.crud.repositories.WorkedHoursRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class WorkedHoursService {

    @Autowired
    private WorkedHoursRepository workedHoursRepository;

    public void deleteWorkedHours (Long id) throws IOException {
        if (id == null) {
            throw new IOException("There is no worker with this id");
        } else {
            workedHoursRepository.deleteByWorkerId(id);
        }
    }

    public void saveWorkedHours (WorkedHours workedHours) throws IOException {
        if (workedHours == null) {
            throw new IOException("The value is empty!");
        } else {
            workedHoursRepository.save(workedHours);
        }
    }

       public WorkedHours findWorkedHoursById(Long id) {
           if (id == null) {
               return null;
           }
        return workedHoursRepository.findByWorkerId(id).orElseThrow(IllegalArgumentException::new);
    }


    public List<WorkedHours> findAll() {
        return workedHoursRepository.findAll();
    }
}

