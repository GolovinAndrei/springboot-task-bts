package by.golovin.task.crud.repositories;

import by.golovin.task.api.model.Worker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkerRepository extends CrudRepository <Worker, Long> {

    @Override
    List<Worker> findAll();

}
