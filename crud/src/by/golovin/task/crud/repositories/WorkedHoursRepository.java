package by.golovin.task.crud.repositories;

import by.golovin.task.api.model.WorkedHours;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkedHoursRepository extends CrudRepository<WorkedHours, Long> {

    @Override
    List<WorkedHours> findAll();

    Optional<WorkedHours> findByWorkerId(Long workerId);

    void deleteByWorkerId (Long workerId);
}
