package by.golovin.task.crud.repositories;

import by.golovin.task.api.model.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

        @Override
        List<Department> findAll();

}
