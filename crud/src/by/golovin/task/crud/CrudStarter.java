package by.golovin.task.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudStarter {

    public static void main(String[] args) {
        SpringApplication.run(CrudStarter.class, args);
    }
}
