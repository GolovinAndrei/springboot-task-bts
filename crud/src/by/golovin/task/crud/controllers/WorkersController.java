package by.golovin.task.crud.controllers;

import by.golovin.task.api.model.Worker;
import by.golovin.task.crud.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class WorkersController {

    @Autowired
    private WorkerService workerService;

    @PostMapping(value = "/workers")
    public ResponseEntity<?> addWorker (@RequestBody Worker worker) {
        try {
            workerService.saveWorker(worker);
        } catch (IOException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/workers")
    public ResponseEntity<List<Worker>> allWorkers() {
        final List<Worker> workersList = workerService.findAll();

        return workersList != null &&  !workersList.isEmpty()
                ? new ResponseEntity<>(workersList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/workers/{id}")
    public ResponseEntity<Worker> workerById (@PathVariable(name = "id") Long id) {
        final Worker worker = workerService.findWorkerById(id);
        return worker != null
                ? new ResponseEntity<>(worker, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/workers/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
        try {
            workerService.deleteWorker(id);
        } catch (IOException e){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
