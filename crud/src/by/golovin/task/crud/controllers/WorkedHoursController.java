package by.golovin.task.crud.controllers;

import by.golovin.task.api.model.WorkedHours;
import by.golovin.task.crud.service.WorkedHoursService;
import by.golovin.task.crud.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class WorkedHoursController {

    @Autowired
    private WorkedHoursService workedHoursService;

    @Autowired
    private WorkerService workerService;

    @PostMapping(value = "/worked_hours")
    public ResponseEntity<?> addWorkedHours (@RequestBody WorkedHours workedHours) {
        try {
            workedHoursService.saveWorkedHours(workedHours);
        } catch (IOException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/worked_hours")
    public ResponseEntity<List<WorkedHours>> workedHourses() {
        final List<WorkedHours> workedHoursList = workedHoursService.allWorkedHours();
        return workedHoursList != null &&  !workedHoursList.isEmpty()
                ? new ResponseEntity<>(workedHoursList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping(value = "/worked_hours/{workerId}")
    public ResponseEntity<List<WorkedHours>> workedHoursByWorker (@PathVariable(name = "workerId") Long workerId) {
        final List <WorkedHours> hoursList = workerService.ownWorkedHours(workerId);
        return hoursList != null && !hoursList.isEmpty()
                ? new ResponseEntity<>(hoursList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/worked_hours/{workerId}")
    public ResponseEntity<?> deleteWorkedHoursForWorker(@PathVariable(name = "workerId") Long workerId) {
        final List<WorkedHours> hoursList = workerService.ownWorkedHours(workerId);
        if (hoursList != null && !hoursList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        try {
            for (WorkedHours wh : hoursList) {
                workedHoursService.deleteWorkedHours(wh.getId());
            }
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
