package by.golovin.task.crud.controllers;

import by.golovin.task.api.model.Department;
import by.golovin.task.crud.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping(value = "/departments")
    public ResponseEntity<?> addDepartment (@RequestBody Department department) {
        try {
            departmentService.saveDepartment(department);
        } catch (IOException e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/departments")
    public ResponseEntity<List<Department>> departments() {
        final List<Department> departmentsList = departmentService.findAll();
        return departmentsList != null &&  !departmentsList.isEmpty()
                ? new ResponseEntity<>(departmentsList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/departments/{id}")
    public ResponseEntity<Department> workerById (@PathVariable(name = "id") Long id) {
        final Department department = departmentService.findDepartmentById(id);
        return department != null
                ? new ResponseEntity<>(department, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/departments/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
        try {
            departmentService.deleteDepartment(id);
        } catch (IOException e){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

