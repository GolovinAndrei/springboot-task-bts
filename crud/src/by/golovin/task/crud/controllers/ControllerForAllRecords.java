package by.golovin.task.crud.controllers;

import by.golovin.task.api.model.Department;
import by.golovin.task.api.model.WorkedHours;
import by.golovin.task.api.model.Worker;
import by.golovin.task.crud.service.DepartmentService;
import by.golovin.task.crud.service.WorkedHoursService;
import by.golovin.task.crud.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ControllerForAllRecords {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private WorkedHoursService workedHoursService;

    @Autowired
    private WorkerService workerService;


    @GetMapping(value = "/all")
    public ResponseEntity<?> departments() {
        List<List> allRecordsFromDB = new ArrayList<>();
        final List<Department> departmentsList = departmentService.findAll();
        final List<WorkedHours> workedHoursList = workedHoursService.findAll();
        final List<Worker> workersList = workerService.findAll();
        allRecordsFromDB.add(departmentsList);
        allRecordsFromDB.add(workedHoursList);
        allRecordsFromDB.add(workersList);
        return !allRecordsFromDB.isEmpty()
                ? new ResponseEntity<>(departmentsList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
