insert into departments (name) values ('ASU TP');
insert into departments (name) values ('OMTS');
insert into worked_hours (start, finish, worker) values ('2021-06-15T09:00:00Z', '2021-06-15T11:00:00Z', 1);
insert into worked_hours (start, finish, worker) values ('201-05-10T07:00:00Z', '2021-05-10T010:00:00Z', 2);
insert into workers (name, last_name, department, position) values ('Anton','Zaharov', 1,'engineer');
insert into workers (name, last_name, department, position) values ('Andrei','Popov', 2,'manager');
