package by.golovin.task.api.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "worked_hours")
public class WorkedHours {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date start;

    @Column
    private Date finish;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "worker")
    private Worker worker;

    public WorkedHours() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkedHours)) return false;
        WorkedHours that = (WorkedHours) o;
        return id.equals(that.id) &&
                start.equals(that.start) &&
                finish.equals(that.finish) &&
                worker.equals(that.worker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, start, finish, worker);
    }
}
