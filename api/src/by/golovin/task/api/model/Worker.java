package by.golovin.task.api.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "workers")
public class Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private String lastName;

    @Column
    private String position;


    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "department")
    private Department department;

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<WorkedHours> workedHours = new ArrayList<>();

    public Worker() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<WorkedHours> getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(List<WorkedHours> workedHours) {
        this.workedHours = workedHours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Worker)) return false;
        Worker worker = (Worker) o;
        return id.equals(worker.id) &&
                name.equals(worker.name) &&
                lastName.equals(worker.lastName) &&
                department.equals(worker.department) &&
                position.equals(worker.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, department, position);
    }
}
